package epam.com.domain;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class OrderTest {

    @Test
    void buildOrder_whenUsebuilder_thenCorrectOrderIsCreated() {

        Order order = Order.builder()
                .orderId("id")
                .cheese()
                .mushrooms()
                .pepper()
                .pork()
                .size(PizzaSize.STANDARD)
                .build();

        assertEquals("id", order.getOrderId());
        assertEquals(PizzaSize.STANDARD, order.getSize());
        assertTrue(order.isCheese());
        assertTrue(order.isMushrooms());
        assertTrue(order.isPepper());
        assertTrue(order.isPork());
    }
}
