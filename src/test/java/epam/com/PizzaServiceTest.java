package epam.com;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class PizzaServiceTest {

    @Test
    void getService_whenCalledTwoTimes_thenSameObjected_IsReturned() {
        PizzaService pizzaService1 = PizzaService.getService();
        PizzaService pizzaService2 = PizzaService.getService();
        assertTrue(pizzaService1 == pizzaService2);
    }
}
