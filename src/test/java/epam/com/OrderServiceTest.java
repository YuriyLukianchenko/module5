package epam.com;

import epam.com.domain.Order;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.concurrent.ArrayBlockingQueue;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class OrderServiceTest {

    @Mock
    PizzaService pizzaService;

    @Mock
    ArrayBlockingQueue<Order> queue;

    @BeforeEach
    void init(){
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void getService_whenCalledTwoTimes_thenSameObjected_IsReturned() {
        OrderService orderService1 = OrderService.getService(pizzaService);
        OrderService orderService2 = OrderService.getService(pizzaService);
        assertTrue(orderService1 == orderService2);
    }

    @Test
    void submitOrder_whenOrderIsSubmitted_thenPizzaServiceStartWorking() {
        Order order = Order.builder().orderId("my order").cheese().mushrooms().build();

        when(pizzaService.getOrderQueue()).thenReturn(queue);

        OrderService orderService = OrderService.getService(pizzaService);
        orderService.submitOrder(order);

        verify(pizzaService).startWork();
    }



}
