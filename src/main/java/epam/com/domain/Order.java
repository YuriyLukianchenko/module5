package epam.com.domain;

public class Order {

    private String orderId;
    private PizzaSize size;
    private boolean cheese;
    private boolean pork;
    private boolean pepper;
    private boolean mushrooms;

    private Order() {
    }

    public String getOrderId() {
        return orderId;
    }
    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }
    public PizzaSize getSize() {
        return size;
    }
    public void setSize(PizzaSize size) {
        this.size = size;
    }
    public boolean isCheese() {
        return cheese;
    }
    public void setCheese(boolean cheese) {
        this.cheese = cheese;
    }
    public boolean isPork() {
        return pork;
    }
    public void setPork(boolean pork) {
        this.pork = pork;
    }
    public boolean isPepper() {
        return pepper;
    }
    public void setPepper(boolean pepper) {
        this.pepper = pepper;
    }
    public boolean isMushrooms() {
        return mushrooms;
    }
    public void setMushrooms(boolean mushrooms) {
        this.mushrooms = mushrooms;
    }

    public static Builder builder() {
        return new Order().new Builder();
    }
    public class Builder {

        private Builder() {
        }

        public Builder orderId(String orderId) {
            Order.this.orderId = orderId;
            return this;
        }
        public Builder size(PizzaSize size) {
            Order.this.size = size;
            return this;
        }
        public Builder cheese() {
            Order.this.cheese = true;
            return this;
        }
        public Builder pork() {
            Order.this.pork = true;
            return this;
        }
        public Builder pepper() {
            Order.this.pepper = true;
            return this;
        }
        public Builder mushrooms() {
            Order.this.mushrooms = true;
            return this;
        }

        public Order build() {
            return Order.this;
        }
    }


}
