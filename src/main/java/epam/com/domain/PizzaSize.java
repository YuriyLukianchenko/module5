package epam.com.domain;

public enum PizzaSize {
    SMALL, STANDARD, BIG
}
