package epam.com;

import epam.com.domain.Order;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.atomic.AtomicBoolean;


public class PizzaService {
    int capacity = 10;
    private static final org.apache.logging.log4j.Logger log =
            org.apache.logging.log4j.LogManager.getLogger(PizzaService.class);
    private static boolean firstTime = true;
    private ArrayBlockingQueue<Order> orderQueue = new ArrayBlockingQueue<>(capacity);
    private AtomicBoolean isWorking = new AtomicBoolean();
    private static PizzaService pizzaService;

    public ArrayBlockingQueue<Order> getOrderQueue() {
        return orderQueue;
    }

    private PizzaService() {

    }

    public void startWork() {
        if (!isWorking.get()) {
            isWorking.set(true);
            Thread pizzaThread = new Thread(() -> preparePizza());
            pizzaThread.start();
        } else {
            log.info("PizzaService already working");
            isWorking.set(false);
        }
    }

    private void preparePizza() {
        int timeTosleep = 10_000;
        while(!orderQueue.isEmpty()) {
            try {
                Order order = orderQueue.take();
                Thread.currentThread().sleep(timeTosleep);
                log.info(String.format("Order %s is completed", order.getOrderId()));
                isWorking.set(!orderQueue.isEmpty());
            } catch (InterruptedException e) {
               log.info("taking order from queue was interrupted");
               isWorking.set(false);
            }
        }
    }

    public static PizzaService getService() {

        if (firstTime) {
            pizzaService = new PizzaService();
            firstTime = false;
        }
        return pizzaService;
    }
}
