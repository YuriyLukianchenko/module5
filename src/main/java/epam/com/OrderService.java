package epam.com;

import epam.com.domain.Order;

public class OrderService {
    private static final org.apache.logging.log4j.Logger log =
            org.apache.logging.log4j.LogManager.getLogger(OrderService.class);
    private static boolean firstTime = true;
    private static OrderService orderService;
    private PizzaService pizzaService;

    private OrderService(PizzaService pizzaService) {
        this.pizzaService = pizzaService;
    }

    public void submitOrder(Order order) {
        try {
            pizzaService.getOrderQueue().put(order);
            pizzaService.startWork();
        } catch (InterruptedException e) {
            log.info("Failed to put order to queue");
        }
    }

    public static OrderService getService(PizzaService pizzaService) {

        if (firstTime) {
            orderService = new OrderService(pizzaService);
            firstTime = false;
        }
        return orderService;
    }
}
