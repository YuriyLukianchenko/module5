package epam.com;

import epam.com.domain.Order;

public class app {
    public static void main(String[] args) {
        PizzaService pizzaService = PizzaService.getService();
        OrderService orderService = OrderService.getService(pizzaService);

        Order order1 = Order.builder().orderId("1").build();
        Order order2 = Order.builder().orderId("2").build();

        orderService.submitOrder(order1);
        orderService.submitOrder(order2);

    }
}
